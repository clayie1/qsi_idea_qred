# qsi_idea_qred
# clayie1
# oct 2018

plotting for IDEA qRED analysis

## overview
1. take adapted qRED score data (paste to idea_qRED.txt) for all idea projects and plot out for clarity
2. compare different qRED features for usefulness/discrimination
3. retrospectively try to classify projects we did/didn't get involved with

